The avatar is from [Wikimedia](https://commons.wikimedia.org/wiki/File:Okonjima_Lioness.jpg), licensed under the [CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/deed.en). I cropped it with GIMP because apparently Gitlab didn't like the idea of making it fill the whole square avatar space.

Technically I think I might not be exactly legal as I'm not sure what the permissions for the plane model are (why didn't I check that when I downloaded it? lol) and I downloaded the sound track directly off of some random website years ago.

Also thanks to:
* Chris McCormick for making [PodSixNet](https://github.com/chr15m/PodSixNet)
* The [creators of Pygame](https://github.com/pygame/pygame#credits) for making, well, [Pygame](https://www.pygame.org/)
* A bunch of other people