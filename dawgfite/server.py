from __future__ import division, absolute_import, print_function

import random
import pygame.time
from weakref import WeakKeyDictionary

import numpy as np

from utilities import *
from version import __version__

from PodSixNet.Server import Server
from PodSixNet.Channel import Channel

NAME_PLACEHOLDER = "An unidentified player"

current_fps = 100.0

class ClientChannel(Channel):
    """ This is the server representation of a single connected client. """

    def __init__(self, *args, **kwargs):

        Channel.__init__(self, *args, **kwargs)
        self.name = NAME_PLACEHOLDER

        self.score = 0
        self.won = False  # did we win?

    def Close(self):
        self._server.DelPlayer(self)

    def Network_data(self, data):
        """ Get the attributes of the player. """

        # If it's the first time getting data we need to do some stuff
        if self.name == NAME_PLACEHOLDER:
            self.name = data["name"]

            print(self.name + " joined the server")

            self._server.SendPlayerStatus(self.name, "joined")
            self.Pump()
            pygame.time.wait(1)  # Allow some delay to process the data
            self._server.Pump()

            self._server.CheckGameStart()

        self._server.SendToAll(data)

        # Grab the score if it exists
        if "score" in data:
            self.score = data["score"]


class DawgfiteServer(Server):
    channelClass = ClientChannel

    def __init__(self, *args, **kwargs):

        global HW

        print("Welcome to DawgFite " + __version__)

        host = kwargs["localaddr"][0]
        port = kwargs["localaddr"][1]
        Server.__init__(self, *args, **kwargs)
        print("Server launched on " + str(host) + ":" + str(port))

        self.players = WeakKeyDictionary()

        self.num_players = ask_number("\nHow many players are you expecting? ")
        print("Waiting for players to join...")

        self.island_poses = []
        for thingy in [-1, 1]:
            x = (ISLAND_RADIUS + ISLAND_DIST) * thingy
            pos = (x, 0.0, 0.0)
            self.island_poses.append(pos)

        self.game_started = False
        self.sent_end = False

        self.frames_played = 0
        self.clock = pygame.time.Clock()

    def Connected(self, channel, addr):
        self.AddPlayer(channel)

    def AddPlayer(self, player):
        if len(self.players) < self.num_players:
            self.players[player] = True

    def CheckGameStart(self):

        names = [player.name for player in self.players]

        # Quit if two players have the same name
        for player in self.players:
            if (names.count(player.name) > 1) and player.name != NAME_PLACEHOLDER:
                message = "More than one player named " + p.name + ".\nThis means you will have to restart."
                print("Fatal error:", message)
                self.SendToAll({"action": "error", "error": message})
                self.Pump()
                exit()

        if (len(self.players) == self.num_players) and (NAME_PLACEHOLDER not in names):
            self.game_started = True

            vehicle_data = {}
            for i, player in enumerate(self.players):
                # Make an adapted version of client.Plane.reset_plane_data
                data = {"name": player.name, "respawning": -1, "health": 100.0, "sender": None}

                # Out, up, right
                # We need numpy arrays here so that we can calculate right
                data["out"] = np.array((random.random(), 0.0, random.random()))  # I hope
                data["up"] = np.array((0.0, 1.0, 0.0))  # Apparently this isn't the convention. I'll have to fix that some day.
                data["right"] = normalize(np.cross(data["up"], data["out"]))

                # Now convert them all back to tuples
                data["out"] = a2tf(data["out"])
                data["up"] = a2tf(data["up"])
                data["right"] = a2tf(data["right"])

                # Color
                data["color"] = (random.random(), random.random(), random.random())

                # Vehicle type and spawn location
                index = i % 2
                if index == 0:  # It's a plane
                    data["island_index"] = i % 2  # Will be either 1 or 0
                else:  # It's a ship
                    data["island_index"] = None

                vehicle_data[player.name] = data

            self.SendToAll({"action": "startgame",
                            "num_players": self.num_players,
                            "vehicle_data": vehicle_data,
                            "island_poses": self.island_poses})
            self.Pump()

            self.timer = GAME_TIME

    def DelPlayer(self, player):

        print(player.name + " is leaving the game.")

        del self.players[player]
        self.SendPlayerStatus(player.name, "left")

        # If all the players leave, then we want to quit
        if len(self.players) <= 0 and self.game_started:
            exit()

    def SendPlayerStatus(self, name, status):

        if status not in ("joined", "left"):
            raise ValueError("status must be \"joined\" or \"left\"")

        self.SendToAll({"action": "playerstatus",
                        "name": name,
                        "status": status})

    def SendToAll(self, data):

        for p in self.players:
            p.Send(data)

    def SendToName(self, name, data):

        for player in self.players:
            if player.name == name:
                player.Send(data)

    def Launch(self):

        global current_fps

        while True:
            self.Pump()

            self.frames_played += 1
            if hasattr(self, "timer"):
                self.timer -= 1.0 / current_fps

                if self.timer <= 0.0 and not self.sent_end:
                    scores = [player.score for player in self.players]
                    winning_score = max(scores)
                    winners = []

                    # Check for winner(s)
                    for player in self.players:
                        if player.score == winning_score:
                            winners.append(player.name)

                    print(win_message(winners, winning_score))

                    self.SendToAll({"action": "end_game", "winners": winners, "winning_score": winning_score})

                    self.sent_end = True

            self.clock.tick(100.0)
            if self.frames_played > 50:
                current_fps = self.clock.get_fps()


def main(host, port, args):

    if args.low_performance:
        set_low_performance()

    s = DawgfiteServer(localaddr = (host, port))
    s.Launch()

