from __future__ import division, absolute_import, print_function

import sys
import math
import random
import subprocess
from time import sleep, time

import numpy as np

import pygame
from pygame.locals import *

from OpenGL.GL import *
from OpenGL.GLU import *

from PodSixNet.Connection import connection, ConnectionListener

from utilities import *
from version import __version__

INITIAL_FPS = 50
current_fps = INITIAL_FPS  # A var that keeps track of the FPS, updated every frame

END_TIME = 3.0  # How long the game lasts after it ends (in seconds)

HEIGHT = 450  # Height of the world

pygame.init()   # initialize pygame

def window2view(pts):
    """
    Convert window coordinates to 3D coordinates.
    """

    model = glGetDoublev(GL_MODELVIEW_MATRIX)
    projection = glGetDoublev(GL_PROJECTION_MATRIX)
    viewport = glGetIntegerv(GL_VIEWPORT)

    retval = [gluUnProject(pt[0], pt[1], 0.001, model, projection, viewport) for pt in pts]

    return retval

def exec_raw(full_name):
    """
    Read in a triangular representation of a piece for rendering.
    Used a home-grown format which was much faster than stl or ogl.gz
    reading.
    """

    rawdata = np.fromfile(full_name, np.float32)

    raw_length = len(rawdata) // 2

    normals = np.reshape(rawdata[raw_length:], (raw_length // 3, 3))
    vertices = np.reshape(rawdata[:raw_length], (raw_length // 3, 3))

    glEnableClientState(GL_VERTEX_ARRAY)
    glEnableClientState(GL_NORMAL_ARRAY)

    glVertexPointerf(vertices)
    glNormalPointerf(normals)
    glDrawArrays(GL_TRIANGLES, 0, len(vertices))

    glDisableClientState(GL_VERTEX_ARRAY)
    glDisableClientState(GL_NORMAL_ARRAY)

def draw_font(font, pos, color, text):
    """ Remember that pos is the pos before window2view is called. """

    font_surface = font.render(str(text), True, color)
    data = pygame.image.tostring(font_surface, "RGBA", True)

    glPushMatrix()
    glLoadIdentity()
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
    glEnable(GL_BLEND)
    glDisable(GL_LIGHTING)

    glColor(HUD.FONT_COLOR)
    glRasterPos(*window2view([pos])[0])
    glDrawPixels(font_surface.get_width(), font_surface.get_height(), GL_RGBA, GL_UNSIGNED_BYTE, data)

    glEnable(GL_LIGHTING)
    glDisable(GL_BLEND)
    glPopMatrix()

def get_font_size(text, size, font_file = "../data/fonts/DejaVuSans.ttf"):
    return pygame.font.Font(font_file, size).size(text)

def setup_explosion():

    if Explosion.gllists == "Unknown":
        Explosion.gllists = []

        for i in range(Explosion.NO_EXPLOSION_FRAMES):
            # New formula, I hope it works :P
            name = "../data/models/explosions/plane_explosion_000000"[:-len(str(i + 1))] + str(i + 1) + ".raw"
            # print("added", name)
            # name = "../data/models/explosions/monkey.raw"

            gllist = glGenLists(1)
            glNewList(gllist, GL_COMPILE)
            exec_raw(name)
            glEndList()

            Explosion.gllists.append(gllist)

def align_ends(pos, out, up, right, rotate = 0.0, against = False):
    """
    Aligns from_end to to_end and spins by rotate and holds that matrix.

    Messes up the matrix.  Use glPushMatrix()/glPopMatrix() before and after if
    you want it preserved.
    """

    fout = np.array((0.0, 0.0, 1.0))
    fup = np.array((0.0, 1.0, 0.0))
    fright = np.array((1.0, 0.0, 0.0))

    # Rotate to identity
    rotate2identity = np.array([
            [fout[0], fout[1], fout[2], 0.0],
            [fup[0], fup[1], fup[2], 0.0],
            [fright[0], fright[1], fright[2], 0.0],
            [0.0, 0.0, 0.0, 1.0]])

    # Rotate to to_end
    if against:
        out = -out
    rotate2to_end = np.array([
            [out[0], out[1], out[2], 0.0],
            [up[0], up[1], up[2], 0.0],
            [right[0], right[1], right[2], 0.0],
            [0.0, 0.0, 0.0, 1.0]])

    # Aligns piece to identity at origin, then flips, then aligns
    # identity to to_end
    glTranslatef(*pos)
    glMultMatrixf(rotate2to_end)
    glRotatef(rotate, 1.0, 0.0, 0.0)
    glMultMatrixf(np.transpose(rotate2identity))

class Tree(Shape):
    """ A dead tree. Leafless. *sniff* """

    COLOR = (0.671, 0.447, 0.282)

    # LOD vars (meters)
    LP_DIST = 80
    FAR_DIST = 300

    def __init__(self, pos, player):

        if Tree.gllist == "Unknown":
            Tree.gllist = glGenLists(1)
            glNewList(Tree.gllist, GL_COMPILE)
            exec_raw("../data/models/tree/tree_dead.raw")
            glEndList()

            Tree.lp_gllist = glGenLists(1)
            glNewList(Tree.lp_gllist, GL_COMPILE)
            exec_raw("../data/models/tree/tree_lowpoly.raw")
            glEndList()

            Tree.far_gllist = glGenLists(1)
            glNewList(Tree.far_gllist, GL_COMPILE)
            exec_raw("../data/models/tree/tree_far.raw")
            glEndList()

        self.player = player
        self.pos = np.array(pos)
        self.alive = True

    def update(self):

        player_dist = mag(self.player.data["pos"] - self.pos)

        glPushMatrix()
        glColor(*Tree.COLOR)
        glTranslate(*self.pos)

        if player_dist < Tree.LP_DIST:
            glCallList(Tree.gllist)
        elif player_dist >= Tree.LP_DIST and player_dist < Tree.FAR_DIST:
            glCallList(Tree.lp_gllist)
        else:
            glCallList(Tree.far_gllist)

        glPopMatrix()


class Ocean(Shape):
    """ A plane that will serve as the ocean. """

    COLOR = (0.129, 0.596, 0.831)
    WATER_TEXTURE = "../data/textures/water.png"
    HW = 5000  # super big!

    def __init__(self):
        self.pos = np.array((0.0, 0.0, 0.0))
        self.alive = True
        self.gen_list()

    def gen_list(self):

        glEnable(GL_TEXTURE_2D)

        self.texture = glGenTextures(1)
        glBindTexture(GL_TEXTURE_2D, self.texture)
        image = pygame.image.load(Ocean.WATER_TEXTURE)
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA,
                     image.get_width(),
                     image.get_height(),
                     0, GL_RGBA, GL_UNSIGNED_BYTE,
                     pygame.image.tostring(image, "RGBX", 1))
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)

        glPushMatrix()
        glLoadIdentity()

        Ocean.gllist = glGenLists(1)
        glNewList(Ocean.gllist, GL_COMPILE)

        # Turn on alpha blending
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
        glEnable(GL_BLEND)

        glColor(1.0, 1.0, 1.0)
        glDisable(GL_LIGHTING)
        glBindTexture(GL_TEXTURE_2D, self.texture)

        glBegin(GL_QUADS)
        glNormal(0.0, 1.0, 0.0)
        glTexCoord2f(0.0, 0.0)
        glVertex(self.pos[0] - Ocean.HW,
                 self.pos[1],
                 self.pos[2] - Ocean.HW)
        glNormal(0.0, 1.0, 0.0)
        glTexCoord2f(0.0, 1.0)
        glVertex(self.pos[0] + Ocean.HW,
                 self.pos[1],
                 self.pos[2] - Ocean.HW)
        glNormal(0.0, 1.0, 0.0)
        glTexCoord2f(1.0, 1.0)
        glVertex(self.pos[0] + Ocean.HW,
                 self.pos[1],
                 self.pos[2] + Ocean.HW)
        glNormal(0.0, 1.0, 0.0)
        glTexCoord2f(1.0, 0.0)
        glVertex(self.pos[0] - Ocean.HW,
                 self.pos[1],
                 self.pos[2] + Ocean.HW)
        glNormal(0.0, 1.0, 0.0)
        glEnd()

        glEnable(GL_LIGHTING)
        glDisable(GL_BLEND)

        glEndList()
        glPopMatrix()

        glDisable(GL_TEXTURE_2D)

    def update(self):

        glPushMatrix()
        glEnable(GL_TEXTURE_2D)
        glColor(*Ocean.COLOR)
        glCallList(Ocean.gllist)
        glDisable(GL_TEXTURE_2D)
        glPopMatrix()


class Island(Shape):
    """ Each team has a island that's important to them. """

    COLOR = (0.282, 0.729, 0.243)
    # OpenGL Units - distance from adjacent vertices in the grid
    # one sixty-fourth times 500 is:
    GRID_SIZE = 7.8125

    def __init__(self, pos):

        self.pos = np.array(pos)
        self.alive = True

        if Island.gllist == "Unknown":
            Island.gllist = glGenLists(1)
            glNewList(Island.gllist, GL_COMPILE)
            glPushMatrix()
            exec_raw("../data/models/island/island_big.raw")
            glPopMatrix()
            glEndList()

    # This is weird, not putting selfs in
    def round(number):
        """ Round number to the nearest Island.GRID_SIZE. """

        return Island.GRID_SIZE * round(number / Island.GRID_SIZE)

    def dist_y(self, pos):
        """ Return the vertical distance between a position and the island. """

        pos = np.array(pos)  # This will ensure that pos is an array and make sure we don't corrupt the original pos
        # Move the pos so there is an effect that the island pos is at the origin
        pos -= self.pos
        pos_2d = [Island.round(pos[0]), Island.round(pos[2])]

        if pos_2d in Island.VERTICES_2D:
            vertex_3d = Island.VERTICES[Island.VERTICES_2D.index(pos_2d)]
            return pos[1] - vertex_3d[1]

        # If it's not over the island, then it must be over sea
        return pos[1]

    def update(self):
        glPushMatrix()
        glColor(*Island.COLOR)
        glTranslate(*self.pos)
        glCallList(self.gllist)
        glPopMatrix()

    def setup():
        """ Read the obj file and set a list of 2D positions. """

        getfile = open("../data/models/island/island_big.obj", "r")
        file_lines = getfile.readlines()
        getfile.close()

        Island.VERTICES = []
        Island.VERTICES_2D = []

        for line in file_lines:
            line = line.strip()
            if line[:2] == "v ":
                coordinates = line.split()[1:] # remove the "v " from the list
                # Convert strings in coordinates to floats
                vertex = []
                for i, coordinate in enumerate(coordinates):
                    coordinate = float(coordinate)
                    if i != 1:
                        coordinate = Island.round(coordinate)
                    vertex.append(coordinate)

                Island.VERTICES.append(vertex)
                Island.VERTICES_2D.append([vertex[0], vertex[2]])


class Explosion(object):

    SOUND = pygame.mixer.Sound("../data/sound/explosion.wav")

    NO_EXPLOSION_FRAMES = 100

    gllists = "Unknown"

    def __init__(self, pos, out, color):

        Explosion.SOUND.play()

        self.pos = pos.copy()
        self.out = np.array(out)
        self.color = color

        self.frame_index = 0
        self.alive = True

    def update(self):
        glPushMatrix()
        glColor(*self.color)
        glTranslate(*self.pos)
        glCallList(Explosion.gllists[self.frame_index])
        glPopMatrix()

        self.frame_index += 1
        if self.frame_index >= Explosion.NO_EXPLOSION_FRAMES:
            self.alive = False

        self.pos += self.out * Plane.SPEED / current_fps

    def update_fps(self):
        pass


class Bullet(Shape):
    """ Here it is. The whole point of the game. """

    SPEED = 50.0
    DAMAGE = 8.0  # The damage per bullet

    # In OpenGL units
    LENGTH = 500.0
    GUN_SIDE = 2        # for bullet alignment
    START_DISTANCE = 1  # also for bullet alignment

    LIFETIME = 0.09  # seconds

    COLOR = (1.0, 0.3, 0.1)

    SOUND = pygame.mixer.Sound("../data/sound/bullet.wav")

    def __init__(self, pos, out, up, name):

        if Bullet.gllist == "Unknown":
            Bullet.gllist = glGenLists(1)
            glNewList(Bullet.gllist, GL_COMPILE)
            exec_raw("../data/models/bullet/bullet.raw")
            glEndList()

        # Make noise
        Bullet.SOUND.play()

        self.pos = np.array(pos)
        self.original_pos = np.array(pos)  # DON'T do self.pos here, I think

        self.out = np.array(out)
        self.up = np.array(up)
        self.right = np.cross(self.up, self.out)

        self.pos -= 2.0 * normalize(self.up)  # Position the bullet so that it's coming out of the gun

        self.name = name  # Who shot the shell
        self.id = id(self)  # A special ID unique to each bullet

        self.life = Bullet.LIFETIME  # Until I find how I'm going to do this
        self.alive = True

    def update(self):

        self.life -= 1.0 / current_fps
        if self.life <= 0:
            self.alive = False

        glPushMatrix()
        align_ends(self.pos, self.out, self.up, self.right)
        glColor(*Bullet.COLOR)
        glCallList(Bullet.gllist)
        glPopMatrix()

        self.pos += normalize(self.out) * (Bullet.SPEED / current_fps)

    def collide_plane(self, plane):
        """ Check for a collision between an bullet and a plane. """

        x_0 = plane.data["pos"]  # Enemy plane position
        x_1 = self.pos  # Plane that's shooting's position
        x_2 = self.pos + self.out  # Point along the bullet vector

        # http://mathworld.wolfram.com/Point-LineDistance3-Dimensional.html
        distance = np.linalg.norm(np.cross(x_0 - x_1, x_0 - x_2)) / np.linalg.norm(x_2 - x_1)

        if distance < plane.RADIUS:
            return True

        return False


class Vehicle(Shape):
    """
    Base class for a vehicle that is manually controlled by one player.

    Required attributes of a subclass:
    * reset_data()
    * check_keys()
    * self.data needs some keys in it
    * self.gllist
    * update() ought to exist but I'm not sure if it's required or not
    * I'm still discovering new ones all the time
    """

    def __init__(self, name, game):

        Shape.__init__(self)

        self.game = game
        self.data = {"name": name,
                     "keys": (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
                     "score": 0}

        # Are we the player that is running on this client's computer?
        self.player = False

        self.reloading = 0
        self.respawning = -1.0  # seconds. It's -1.0 when we're alive
        self.prevscore = 0      # For the scoring system

    def base_update(self):
        """ Call this method at the beginning of update(). """

        self.reloading -= 1.0 / current_fps
        if self.respawning > 0.0:
            self.respawning -= 1.0 / current_fps

        # Once the respawn wait time is over
        if self.respawning <= 0.0 and self.respawning > -1.0 and self.player:  # Won't work if less than 1 FPS
            self.respawn()

        # If we die
        if self.data["health"] <= 0 and self.respawning == -1.0:
            self.respawning = self.RESPAWN_TIME
            self.data["score"] += self.SCORE_DEATH  # Decreases the score because SCORE_DEATH is negative
            # Make an explosion
            self.game.shapes.explosions.append(Explosion(self.data["pos"],
                                                         self.data["out"],
                                                         self.data["color"]))
            # Make a message
            if self.player:
                self.game.shapes.hud.messages.append([HUD.RESPAWN_STR + str(self.RESPAWN_TIME), self.RESPAWN_TIME])

        if self.respawning == -1.0: # If we're alive:
            glPushMatrix()
            glColor(*self.data["color"])
            # Make the model face the right way
            align_ends(self.data["pos"], self.data["out"], self.data["up"], self.data["right"])

            if hasattr(self, "lp_gllist") and (not self.player) and mag(self.data["pos"] - self.game.shapes.player.data["pos"]) >= self.LP_DIST:
                glCallList(self.lp_gllist)
            else:
                glCallList(self.gllist)
            glPopMatrix()

            self.data["pos"] += self.data["out"] * self.SPEED / current_fps

            if self.player:
                keys = pygame.key.get_pressed()
                self.check_keys(keys)
                self.prevkeys = keys
            else:
                self.check_keys(self.data["keys"])

        # Prevent health and score from becoming negative
        self.data["health"] = max(self.data["health"], 0)
        self.data["score"] = max(self.data["score"], 0)

        if (self.data["score"] != self.prevscore) and self.player:
            self.game.send_data({"name": self.data["name"], "score": self.data["score"]})

        self.prevscore = self.data["score"]

    def recv_data(self, newdata):

        # We shouldn't receive data sent from our instance of the program
        if newdata["sender"] == self.game.name:
            return

        newdata = newdata.copy()

        for key in newdata:
            # if key is a 3-vector we want to convert it to a numpy array
            if (type(newdata[key]) == type(tuple()) or type(newdata[key]) == type(list())) and len(newdata[key]) == 3:
                newdata[key] = np.array(newdata[key])

            if key == "respawning":
                self.respawning = -1.0
            elif key == "damager":
                if self.data["health"] < 0:
                    if newdata["damager"] in self.game.shapes.planes:
                        damager_pos = self.game.shapes.planes[newdata["damager"]].data["pos"]
                    else: # Must be a ship
                        damager_pos = self.game.shapes.ships[newdata["damager"]].data["pos"]
                    distance = str(int(round(mag(self.game.shapes.planes[newdata["damager"]].data["pos"] - self.data["pos"]))))
                    self.game.shapes.hud.info.append([newdata["damager"] + " eliminated " + self.data["name"] + " (" + distance + "m)", 3.0])

                    if self.player:
                        self.game.shapes.hud.messages.append(["Eliminated by " + newdata["damager"], 3.0])
            else:
                self.data[key] = newdata[key]

    def new_data(self, name = None):
        """
        Return a new set of data for a vehicle.

        Return values are numpy arrays if appropriate."""

        if name is None and "name" in self.data:
            name = self.data["name"]

        ret = {"name": name, "respawning": -1, "health": Plane.HEALTH, "sender": None}

        # Out, up, right
        # We need numpy arrays here so that we can calculate right
        ret["out"] = np.array((random.random(), 0.0, random.random()))  # I hope
        ret["up"] = np.array((0.0, 1.0, 0.0))  # Apparently this isn't the convention. I'll have to fix that some day.
        ret["right"] = normalize(np.cross(ret["up"], ret["out"]))

        return ret

    def respawn(self):
        self.respawning = -1.0

        # Get a new set of data, except for the color
        new_data = self.reset_data()

        # Send out the new data
        self.game.send_data(new_data)


class Plane(Vehicle):

    HEALTH = 100.0

    # OpenGL units per second, I think
    SPEED = 60

    # In seconds
    RELOAD_TIME = 0.12
    RESPAWN_TIME = 8.0

    # Damage dealt per frame when a plane tries to exit the playing area
    GROUND_DAMAGE = 1.4 # water/land
    TOP_DAMAGE = 0.05   # maximum altitude
    BORDER_DAMAGE = 0.2 # "walls"

    RADIUS = 9  # for collision detection

    # Pitch, roll, and yaw constants
    # The units of the following movement variables are in radians per second.
    DROT = np.radians(1.0)
    MAX_PITCH = 50.0
    MAX_ROLL = 80.0
    MAX_YAW = 50.0
    PITCH = 5.0
    ROLL = 10.0
    YAW = 4.0

    # Key contstants for standardized key mode
    PUP = pygame.K_UP
    PDOWN = pygame.K_DOWN
    PYLEFT = pygame.K_LEFT
    PYRIGHT = pygame.K_RIGHT
    PRLEFT = pygame.K_x
    PRRIGHT = pygame.K_c

    LP_DIST = 150

    # Here are some constants for the scoring system
    SCORE_HIT = 16      # points per hit scored
    SCORE_KILL = 42     # points per plane killed
    SCORE_DEATH = -20   # points added to the score when you die

    def __init__(self, name, game):

        Vehicle.__init__(self, name, game)

        if Plane.gllist == "Unknown":
            Plane.gllist = glGenLists(1)
            glNewList(Plane.gllist, GL_COMPILE)
            exec_raw("../data/models/plane/plane.raw")
            glEndList()

            Plane.lp_gllist = glGenLists(1)
            glNewList(Plane.lp_gllist, GL_COMPILE)
            exec_raw("../data/models/plane/plane_lowpoly.raw")
            glEndList()

        # For determining whether the bullet comes from the right or left
        self.shots_fired = 0

        # Control variable
        self.pitch = 0.0
        self.roll = 0.0
        self.yaw = 0.0

        self.ip_pitch = 0.0
        self.ip_roll = 0.0
        self.ip_yaw = 0.0

    def __repr__(self):
        return str(self.data)

    def update(self):
        self.base_update()

    def calc_roll(self, angle):
        # out fixed, up/right change
        self.data["up"] = normalize(np.cos(angle)*self.data["up"] + np.sin(angle)*self.data["right"])
        self.data["right"] = normalize(np.cross(self.data["up"], self.data["out"]))

    def calc_pitch(self, angle):
        # right fixed, out/up change
        self.data["out"] = normalize(np.cos(angle)*self.data["out"] + np.sin(angle)*self.data["up"])
        self.data["up"] = normalize(np.cross(self.data["out"], self.data["right"]))

    def calc_yaw(self, angle):
        # up fixed, out/right change
        self.data["out"] = normalize(np.cos(angle)*self.data["out"] + np.sin(angle)*self.data["right"])
        self.right = normalize(np.cross(self.data["up"], self.data["out"]))

    def check_keys(self, keys):

        shift = keys[pygame.K_LSHIFT] or keys[pygame.K_RSHIFT]

        # The number of seconds that have elapsed since last time
        # Hopefully less than one unless your computer is complete garbage
        # Because we can't drop Python 2 support, we have to say 1.0 instead of 1
        elapsed = 1.0 / current_fps

        # Someday I will write a fancy algorithm instead of these million if statements
        if keys[Plane.PUP]:
            # Pitch up
            self.ip_pitch += elapsed * Plane.PITCH
        elif keys[Plane.PDOWN]:
            # Pitch down
            self.ip_pitch -= elapsed * Plane.PITCH
        else:
            self.ip_pitch = 0.0

        if keys[Plane.PYLEFT]:
            self.ip_yaw += elapsed * Plane.YAW
        elif keys[Plane.PYRIGHT]:
            self.ip_yaw -= elapsed * Plane.YAW
        else:
            self.ip_yaw = 0.0

        if keys[Plane.PRLEFT]:
            self.ip_roll -= elapsed * Plane.ROLL
        elif keys[Plane.PRRIGHT]:
            self.ip_roll += elapsed * Plane.ROLL
        else:
            self.ip_roll = 0.0

        if keys[pygame.K_SPACE] and self.respawning == -1.0 and self.reloading <= 0:
            self.game.make_bullet(self)
            self.shots_fired += 1
            self.reloading = Plane.RELOAD_TIME

        # Couldn't there be a way to make this less tedious?
        if self.ip_yaw > 0.0:
            self.ip_yaw = min(self.ip_yaw, elapsed * Plane.MAX_YAW)
        elif self.ip_yaw < 0.0:
            self.ip_yaw = max(self.ip_yaw, elapsed * -Plane.MAX_YAW)
        if self.ip_roll > 0.0:
            self.ip_roll = min(self.ip_roll, elapsed * Plane.MAX_ROLL)
        elif self.ip_roll < 0.0:
            self.ip_roll = max(self.ip_roll, elapsed * -Plane.MAX_ROLL)
        if self.ip_pitch > 0.0:
            self.ip_pitch = min(self.ip_pitch, elapsed * Plane.MAX_PITCH)
        elif self.ip_pitch < 0.0:
            self.ip_pitch = max(self.ip_pitch, elapsed * -Plane.MAX_PITCH)

        self.calc_yaw(Plane.DROT * self.ip_yaw)
        self.calc_roll(-Plane.DROT * self.ip_roll)
        self.calc_pitch(Plane.DROT * self.ip_pitch)

        self.pitch += self.ip_pitch
        self.roll += self.ip_roll
        self.yaw += self.ip_yaw

    def reset_data(self, name = None):
        """
        Set and return pos, out, up, right, color, health for a plane.

        Up will always be straight up, so out and right are the only random
        variables out of the three.

        I'm not converting the values to numpy arrays because of the current
        PodSixNet issues with sending them. (Not issues with PodSixNet, but
        issues that I have to resolve.)
        """

        ret = self.new_data(name = name)
        ret["pos"] = list(self.game.island_poses[self.data["island_index"]])
        ret["pos"][1] = 220.0

        # Set the new data
        self.recv_data(ret)

        # Convert everything to tuples before we return it
        for vector in ("out", "up", "right"):
            ret[vector] = a2tf(ret[vector])

        return ret


class Ship(Vehicle):

    HEALTH = 200

    def __init__(self, pos):
        if Ship.gllist == "Unknown":
            Ship.gllist = glGenLists(1)
            glNewList(Ship.gllist, GL_COMPILE)
            exec_raw("../data/models/ship/ship.raw")
            glEndList()

    def update(self):
        self.base_update()

    def check_keys(self):
        keys = pygame.key.get_pressed()

        if keys[pygame.K_LEFT]:
            print("Turning left")
        elif keys[pygame.K_RIGHT]:
            print("Turning right")

        if keys[pygame.K_UP]:
            print("Looking up")
        elif keys[pygame.K_DOWN]:
            print("Looking down")

    def reset_data(self, name = None):
        ret = self.new_data(name = name)
        ret["pos"] = [0.0, 0.0, 0.0]  # We might change this later

        # Set the new data
        self.recv_data(ret)

        # Convert everything to tuples before we return it
        for vector in ("out", "up", "right"):
            ret[vector] = a2tf(ret[vector])

        return ret


class HUD(object):

    # Health bar
    # Units in pixels
    WIDTH = 6  # How many pixels per health point
    MAX_WIDTH = WIDTH * Plane.HEALTH
    HALF_WIDTH = MAX_WIDTH / 2.0
    HEIGHT = 20
    BOTTOM = 75  # Bottom margin

    # Color of health
    HEALTH_COLOR = (0.180, 0.769, 0.000)
    # Color of not health
    EMPTY_COLOR = (0.604, 0.584, 0.612)

    FONT_SIZE = 25
    FONT_COLOR = (255, 102, 0)  # We could make this equal to the player color if we added a shadow or something

    INFO_SIZE = 20
    MAX_INFO_LENGTH = 5
    INFO_MARGIN = 15
    MESSAGE_SIZE = 40

    RESPAWN_STR = "Respawning in "
    VICTORY_MESSAGE = "You won!"


    def __init__(self, player, screen):

        self.player = player

        # Simply a array of two integers which describes the length and width of the screen
        self.screen = screen

        # Health

        # In absolute terms
        self.left = (self.screen[0] / 2) - HUD.HALF_WIDTH
        self.right = (self.screen[0] / 2) + HUD.HALF_WIDTH

        # Fonts
        self.health_font = pygame.font.Font("../data/fonts/DejaVuSans.ttf", HUD.FONT_SIZE)
        self.message_font = pygame.font.Font("../data/fonts/DejaVuSans.ttf", HUD.MESSAGE_SIZE)
        self.info_font = pygame.font.Font("../data/fonts/DejaVuSans.ttf", HUD.INFO_SIZE)

        self.messages = []
        self.info = []

        self.alive = True

    def draw(self):
        """ Draw the HUD """

        # The following code draws the green part of the bar

        # In relative terms
        greenwidth = HUD.WIDTH * self.player.data["health"]

        if greenwidth > 0:
            # We're going to try bl, tl, tr, br
            pts = np.array([(0.0, 0.0, 0.0),
                            (0.0, HUD.HEIGHT, 0.0),
                            (greenwidth, HUD.HEIGHT, 0.0),
                            (greenwidth, 0.0, 0.0),
                            (0.0, 0.0, 0.0)]) + np.array((self.left, HUD.BOTTOM, 0.0))

            glPushMatrix()
            glLoadIdentity()
            pts = window2view(pts)  # For some reason pts has to be window2view'd exactly in this spot
            glDisable(GL_LIGHTING)

            glColor(*HUD.HEALTH_COLOR)
            glBegin(GL_POLYGON)
            for point in pts:
                glVertex(point)
            glEnd()

            glEnable(GL_LIGHTING)
            glPopMatrix()

        # Now we do the whole thing over again for the empty (gray) part
        graywidth = HUD.MAX_WIDTH - greenwidth

        if graywidth > 0:
            pts = np.array([(0.0, 0.0, 0.0),
                            (0.0, HUD.HEIGHT, 0.0),
                            (graywidth, HUD.HEIGHT, 0.0),
                            (graywidth, 0.0, 0.0),
                            (0.0, 0.0, 0.0)]) + np.array((self.left + greenwidth, HUD.BOTTOM, 0.0))

            glPushMatrix()
            glLoadIdentity()
            pts = window2view(pts)
            glDisable(GL_LIGHTING)

            glColor(*HUD.EMPTY_COLOR)
            glBegin(GL_POLYGON)
            for point in pts:
                glVertex(point)
            glEnd()

            glEnable(GL_LIGHTING)
            glPopMatrix()

        # Font stuff

        # Name
        draw_font(self.health_font, np.array((self.left, HUD.BOTTOM + HUD.HEIGHT)),
                       HUD.FONT_COLOR, str(self.player.data["name"]))

        # Health
        health_text = str(int(round(self.player.data["health"]))) + " / " + str(int(Plane.HEALTH))
        size = get_font_size(health_text, HUD.FONT_SIZE)
        draw_font(self.health_font, np.array((self.screen[0] / 2.0 - size[0] / 2.0, HUD.BOTTOM - size[1])),
                       HUD.FONT_COLOR, health_text)

        # Messages

        # Compile a list of all the message strings
        strings = [message[0] for message in self.messages]
        # A list of messages to be removed
        to_removes = []

        # This will end in .5 if there is an even number of messages
        middle_index = (len(self.messages) - 1) / 2.0

        for i, message in enumerate(strings):
            # Count down if it's a respawning message
            if len(message) > len(HUD.RESPAWN_STR) and message[:len(HUD.RESPAWN_STR)] == HUD.RESPAWN_STR:
                if int(round(self.player.respawning)) < 0:
                    continue
                self.messages[i][0] = HUD.RESPAWN_STR + str(int(round(self.player.respawning)))

            size = get_font_size(message, HUD.MESSAGE_SIZE)
            y = (self.screen[1] / 2.0) + ((middle_index - i) * size[1])
            pos = np.array(((self.screen[0] / 2.0) - size[0] / 2.0, y))

            # Only show messages if the scoreboard isn't showing or the victory message is showing
            if (not pygame.key.get_pressed()[pygame.K_TAB]) or HUD.VICTORY_MESSAGE in strings:
                draw_font(self.message_font, pos, HUD.FONT_COLOR, message)

            # Remove old messages
            self.messages[i][1] -= 1.0 / current_fps
            if self.messages[i][1] < 0.0:
                to_removes.append(self.messages[i])

        for to_remove in to_removes:
            self.messages.remove(to_remove)

        # Info
        # A lot of this code is copied from messages, so maybe I'll have to clean it up one day

        if len(self.info) > HUD.MAX_INFO_LENGTH:
            self.info = self.info[len(self.info) - HUD.MAX_INFO_LENGTH:]

        # Compile a list of all the info strings
        strings = [info[0] for info in self.info]
        # Remove list
        to_removes = []

        for i, info in enumerate(strings):

            size = get_font_size(info, HUD.INFO_SIZE)

            y = self.screen[1] - HUD.INFO_MARGIN - ((i + 1) * HUD.INFO_SIZE)
            pos = np.array((HUD.INFO_MARGIN, y))

            draw_font(self.info_font, pos, HUD.FONT_COLOR, info)

            # Remove old messages
            self.info[i][1] -= 1.0 / current_fps
            if self.info[i][1] < 0.0:
                to_removes.append(self.info[i])

        for to_remove in to_removes:
            self.info.remove(to_remove)


class Scoreboard(object):

    LABELS = ["Name", "Score"]

    FONT_SIZE = 40
    FONT_COLOR = (255, 102, 0)
    TEXT_HEIGHT = get_font_size(LABELS[0], FONT_SIZE)[1]

    WIDTH = 1000
    WIDTH_PADDING = 100
    HEIGHT_PADDING = 20

    COLUMN_WIDTH = 250

    def __init__(self, game):

        self.game = game

        self.font = pygame.font.Font("../data/fonts/DejaVuSans.ttf", Scoreboard.FONT_SIZE)

        # In relative terms
        self.side_margin = (self.game.scr[0] / 2.0) - (Scoreboard.WIDTH / 2.0)

        # A list of x-lefts
        self.lefts = []
        for i in range(2):
            x = self.side_margin + (i * Scoreboard.COLUMN_WIDTH)
            if i != 0:
                x += Scoreboard.WIDTH_PADDING
            self.lefts.append(x)

    def draw(self):

        # A list of lists, each list has a bunch of strings inside of it
        rows = []
        for plane_key in self.game.shapes.planes:
            plane = self.game.shapes.planes[plane_key]
            rows.append([plane.data["name"], int(round(plane.data["score"]))])

        rows.sort(key = lambda row: row[1], reverse = True)

        # Make all the scores strings
        for i in range(len(rows)):  # Is this worse than for i, row in enumerate(rows)?
            rows[i][1] = str(rows[i][1])

        rows.insert(0, Scoreboard.LABELS + [time_format(self.game.timer)])
        rows[-1].append(str(int(round(current_fps))) + " FPS")  # Add FPS to the last row

        middle_index = (len(rows) - 1) / 2.0

        for row_index, row in enumerate(rows):
            for i, label in enumerate(row):

                size = get_font_size(label, Scoreboard.FONT_SIZE)

                if i <= len(self.lefts) - 1: # Prevents error if there is a timer
                    x = self.lefts[i]

                if label == time_format(self.game.timer) or label == str(int(round(current_fps))) + " FPS":
                    x = self.game.scr[0] - self.side_margin - size[0]

                y = (self.game.scr[1] / 2.0) + ((middle_index - row_index) * size[1]) + Scoreboard.HEIGHT_PADDING

                draw_font(self.font, np.array((x, y)), Scoreboard.FONT_COLOR, label)


class ShapeManager(object):
    """
    A class to hold dictionaries of all the shapes.

    Its goal is to prevent the client class from having a bunch of variables
    dedicated to keeping track of all the shapes.
    """

    def __init__(self):

        self.misc = []
        self.islands = []
        self.explosions = []

        # Here, of course, are the positions of natural objects.
        # They will be updated when the time comes.
        self.treeposes = []

        # Now we will have a bunch of dictionaries dedicated to keeping track
        # of a certain type of shape.
        self.planes = {}     # Referenced by name
        self.bullets = {}    # Referenced by ID

    def update(self):
        """
        Update every shape in allshapes.

        If there is a shape that ought to be done last (e.g. HUD), then it will
        be performed in the correct order (hopefully).
        """

        for shape in self.misc + self.explosions + self.islands:
            if shape.alive:
                shape.update()

        for plane in self.planes:
            self.planes[plane].update()

        bullet_removes = []
        for bullet in self.bullets:
            if self.bullets[bullet].alive:
                self.bullets[bullet].update()
            else:
                bullet_removes.append(bullet)
        for bullet in bullet_removes:
            del self.bullets[bullet]

        if hasattr(self, "hud"):
            self.hud.draw()

        if pygame.key.get_pressed()[pygame.K_TAB] and hasattr(self, "scoreboard"):
            self.scoreboard.draw()

    def make_natural(self):
        """Make trees."""

        for pos in self.treeposes:
            self.misc.append(Tree(pos, self.player))


class Client(object):
    """
    A collection of network methods made purely for orginazation

    GameManager is based off of this class so that we don't have to have global
    variables simply for communication reasons.
    """

    def Loop(self):
        connection.Pump()
        self.Pump()

    # SEND METHODS

    def send_data(self, data = None, respawning = False):
        """
        Send a plane's pos, speed, out vector, shooting or not, etc.

        If no argument is given, then keys, pos, out, up, right of self.player
        will be sent.

        We can benefit from the new method of networking by only sending the
        data that changed. Data that didn't change (e.g., color) doesn't have
        to be sent anymore.
        """

        # For debugging
        self.times_sent += 1

        # We will have to do some data formatting because PSN can't send numpy
        # objects, I wonder if there's a better way to do this

        if not data:
            data = {"name": self.name, "keys": pygame.key.get_pressed()}
            for attr in ("pos", "out", "up", "right"):
                # Assuming send_data works correctly, the a2tf here could theoretically be removed
                data[attr] = a2tf(self.shapes.player.data[attr])
        if respawning:
            data["respawning"] = -1.0

        data["action"] = "data"
        data["sender"] = self.name

        # If we don't do .copy() then we corrupt that plane's data
        for key in ("pos, out, up, right"):
            if key in data:
                data[key] = a2tf(data[key])

        connection.Send(data)

    # RECEIVE METHODS

    def Network_playerstatus(self, data):
        """
        Receive a status from another player, "joined" or "left".
        """

        name = data["name"]
        status = data["status"]
        if name != self.name:
            print(data["name"] + " " + status + " the server")

    def Network_startgame(self, data):

        self.island_poses = data["island_poses"]

        self.game_started = True
        self.num_players = data["num_players"]

        self.init_gl_stuff(self.island_poses)

        self.shapes.ocean = Ocean()
        self.shapes.misc.append(self.shapes.ocean)

        # This is totally not confusing. :P
        for name in data["vehicle_data"]:
            self.Network_data(data["vehicle_data"][name])

        # Add the islands that were computed at the server
        for pos in self.island_poses:
            self.shapes.islands.append(Island(pos))

        # create_initial_shapes was kind of a failure
        # Because a lot of the initial shape creation is not done inside of it
        self.create_initial_shapes()

        # Create a timer
        self.timer = GAME_TIME

        while self.frames_til_end < -0.5 or self.frames_til_end > 0.0:
            self.update()

    def Network_data(self, newdata):
        """
        The data passed to this method will eventually reach the vehicle whose
        name is equal to data["name"].
        """

        # Anyway, I decided to do that to minimize the number of global
        # variables that I had made. These variables were simply used for
        # communication between client and main, so now we don't have to have
        # them anymore.

        if not self.game_started:
            return

        # If this is our first time getting data, then we need to add to the
        # planes dict - but only if the game has started
        if newdata["name"] not in self.shapes.planes and newdata["name"] not in self.shapes.vehicles:
            newdata["pos"] = list(self.island_poses[newdata["island_index"]])
            newdata["pos"][1] = 220.0
            plane = Plane(newdata["name"], self)
            self.shapes.planes[newdata["name"]] = plane
            if newdata["name"] == self.name:
                self.shapes.player = plane
                self.shapes.player.player = True
                self.shapes.player.prevkeys = plane.data["keys"]

        self.shapes.planes[newdata["name"]].recv_data(newdata)

    def Network_end_game(self, data):

        message = win_message(data["winners"], data["winning_score"])
        self.shapes.hud.info.append([message, 10.0])
        print("\n" + message + ".")

        if self.shapes.player.data["score"] == data["winning_score"]:
            self.shapes.hud.messages.append([HUD.VICTORY_MESSAGE, 10.0])

        self.frames_til_end = END_TIME

    def Network_error(self, data):

        print("Network error:", data["error"])


class GameManager(ConnectionListener, Client):

    UPDATE_DELAY = 5.0  # seconds
    SKY_COLOR = [0.125, 0.643, 0.863, 1.0]

    def __init__(self, host, port):

        self.Connect((host, port))
        print("Connected to " + str(host) + ":" + str(port) + "\n")

        # Set up the islands before the game starts to avoid freezing when the game starts
        Island.setup()

        max_width = Scoreboard.COLUMN_WIDTH + Scoreboard.WIDTH_PADDING - 10
        width = max_width + 100
        while width > max_width:
            self.name = input23("Please enter your name to continue: ")  # Get the player's name
            width = get_font_size(self.name, Scoreboard.FONT_SIZE)[0]
            if width > max_width:
                print("Sorry, that name is too long")

        # For debugging
        self.times_sent = 0

        # Immediately send our name to the server as soon as possible
        # The advantage of the newer communication system is that it allows you
        # to send only partial attributes instead of compiling a huge list that
        # is overwritten every time.
        connection.Send({"action": "data", "name": self.name, "sender": self.name})

        # Use a class to help us keep track of the shapes
        self.shapes = ShapeManager()

        print("Waiting for other players to join...")

        # check_game_start runs a while loop that doesn't end until the game starts
        self.check_game_start()

    def check_game_start(self):

        # Keep the system busy in this loop until we receive everyone's data
        self.game_started = False
        while not self.game_started:
            self.Loop()
            sleep(0.001)

    def create_initial_shapes(self):

        self.shapes.make_natural()

        self.shapes.hud = HUD(self.shapes.player, self.scr)
        self.shapes.scoreboard = Scoreboard(self)

        # self.shapes.misc.append(Ship((0.0, 0.0, 0.0)))

        self.frames_til_end = -1
        self.frames_played = 0
        # Time until update, in seconds
        self.til_update = random.uniform(1, 3)  # So they don't do it all at the same time intially

    def make_bullet(self, plane):
            temp_out = (2 * plane.data["out"] * Plane.SPEED) / Bullet.SPEED
            temp_pos = np.array(plane.data["pos"]) + Bullet.START_DISTANCE * np.array(plane.data["out"])

            # Find out whether the bullet should come from the left or right
            # If shots_fired is even, it ought to come from the right,
            # otherwise it will come from the left
            if plane.shots_fired % 2 == 0:
                temp_pos += plane.data["right"] * Bullet.GUN_SIDE
            else:
                temp_pos -= plane.data["right"] * Bullet.GUN_SIDE

            bullet = Bullet(temp_pos, temp_out, plane.data["up"], plane.data["name"])
            self.shapes.bullets[bullet.id] = bullet

    def update(self):

        global current_fps

        # Do "gl stuff"
        self.gl_stuff()

        self.frames_played += 1
        if self.frames_til_end > 0:
            self.frames_til_end -= 1.0 / current_fps
        self.til_update -= 1.0 / current_fps
        self.timer -= 1.0 / current_fps

        if self.til_update <= 0:
            self.send_data()
            self.til_update = GameManager.UPDATE_DELAY

        if self.shapes.player.prevkeys != pygame.key.get_pressed():
            self.send_data()

        # Check for collisions
        self.check_collisions()

        # Update all the shapes
        self.shapes.update()

        pygame.display.flip()

        self.clock.tick()
        # We will assume static fps during the initial second
        if self.frames_played >= INITIAL_FPS:
            current_fps = self.clock.get_fps()
        else:
            current_fps = INITIAL_FPS

    def init_gl_stuff(self, ocean_points):

        if hasattr(self, "scr"):
            # init_gl_stuff has already been called, we don't have to do it again
            return

        screen = pygame.display.set_mode((0, 0), OPENGL | DOUBLEBUF | FULLSCREEN | HWSURFACE)
        # screen = pygame.display.set_mode((1280, 720), OPENGL | DOUBLEBUF)
        self.scr = [screen.get_width(), screen.get_height()]

        if self.scr[0] < 1000:
            print("Warning: You have a small screen, this might result in odd behavior")

        # Set the window title
        title = self.name + " | DawgFite " + __version__
        pygame.display.set_caption(title)

        pygame.mouse.set_visible(False) # hide the mouse

        # Sound stuff
        # pygame.mixer.quit()                 # If we don't quit then the re-init is ignored
        # pygame.mixer.init(frequency=48000)  # Without this the theme isn't played right
        pygame.mixer.music.load("../data/sound/theme.mp3")
        pygame.mixer.music.play(-1)     # play the theme song on loop

        # noise = pygame.mixer.Sound("../data/sound/plane.wav")
        # noise.play(-1)  # Annoying plane sound

        # Define tree poses here later
        self.shapes.treeposes = []

        setup_explosion()

        # Random GL stuff and stuff

        # Enable Depth and Turn on Lights
        self.sun_pos = list(ocean_points[0])
        self.sun_pos[1] = HEIGHT * 3
        glEnable(GL_DEPTH_TEST)
        glEnable(GL_LIGHT0)
        glEnable(GL_LIGHTING)
        glLightfv(GL_LIGHT0, GL_POSITION, self.sun_pos)

        # Turn on Colors
        glColorMaterial(GL_FRONT, GL_DIFFUSE)
        glEnable(GL_COLOR_MATERIAL)

        # Set up the camera lens
        glMatrixMode(GL_PROJECTION)

        # The viewing distance will have to be a constant for now, I guess
        gluPerspective(45.0, float(self.scr[0])/float(self.scr[1]), 0.1, 3500)
        glMatrixMode(GL_MODELVIEW)

        # Define the sky color
        glClearColor(*GameManager.SKY_COLOR)

        # make a clock
        self.clock = pygame.time.Clock()

        # We've loaded all the OpenGL stuff, *now* we can check for the start of the game

    def gl_stuff(self):

        pygame.event.pump()
        # Pump connection
        self.Loop()

        # Clear Everything
        glLoadIdentity()
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

        # Set up the observer
        ppos = self.shapes.player.data["pos"]
        pout = self.shapes.player.data["out"]
        pup = self.shapes.player.data["up"]
        at = pout + ppos

        cpos = ppos - (40 * pout)
        # Lift it a  bit
        at += (10 * pup)
        cpos += (10 * pup)

        # We can't do gluLookAt(*pos, *at, *up) in Python 2.
        # https://stackoverflow.com/questions/53000998/unpacking-multiple-function-arguments-in-python-2
        # this means we'll have to do gluLookAt(*(pos + at + up)). And convert everything to lists as well.
        gluLookAt(*list(cpos) + list(at) + list(pup))

        glGetFloatv(GL_MODELVIEW_MATRIX)
        glLightfv(GL_LIGHT0, GL_POSITION, self.sun_pos)

        if pygame.key.get_pressed()[pygame.K_k]:
            # We need a reliable way to get out of it
            print("Killed")
            exit()

        # Quit Game on X
        if pygame.event.get(pygame.QUIT) or pygame.key.get_pressed()[pygame.K_ESCAPE]:
            self.frames_til_end = END_TIME

    def check_collisions(self):

        bullet_removes = []

        for plane_key in self.shapes.planes:
            plane = self.shapes.planes[plane_key]

            # Check if a plane went underwater or over its ceiling
            if plane.data["pos"][1] < 0.0:
                plane.data["pos"][1] = 0.0
                plane.data["health"] -= Plane.GROUND_DAMAGE
            if plane.data["pos"][1] > HEIGHT:
                plane.data["pos"][1] = HEIGHT
                plane.data["health"] -= Plane.TOP_DAMAGE

            # Check if a plane tried to exit the playing area
            pass

            # Check if a plane tried to go inside an island
            for island in self.shapes.islands:
                if island.dist_y(plane.data["pos"]) < (Plane.RADIUS / 2):
                    plane.data["health"] -= Plane.GROUND_DAMAGE

            for bullet_id in self.shapes.bullets:
                bullet = self.shapes.bullets[bullet_id]

                # Only check our own bullets
                if bullet.name != self.name:
                    continue

                if (plane.data["name"] != self.name) and plane.data["health"] > 0 and bullet.collide_plane(plane):
                    bullet.alive = False
                    bullet_removes.append(bullet_id)

                    # Give points to the player who hit the plane
                    self.shapes.player.data["score"] += Plane.SCORE_HIT

                    # Tell the unlucky player that they've been hit
                    plane.data["health"] -= Bullet.DAMAGE
                    self.send_data({"name": plane.data["name"], "health": plane.data["health"], "damager": self.name})

                    if plane.data["health"] <= 0:
                        # Give points for killing
                        # Points for dying are done inside the Plane class
                        self.shapes.player.data["score"] += Plane.SCORE_KILL
                        self.shapes.hud.messages.append(["Eliminated " + plane.data["name"], 3.0])

                        distance = str(int(round(mag(self.shapes.player.data["pos"] - plane.data["pos"]))))
                        self.shapes.hud.info.append([self.name + " eliminated " + plane.data["name"] + " (" + distance + "m)", 3.0])


        for bullet_remove in bullet_removes:
            del self.shapes.bullets[bullet_remove]


def main(host, port, args):

    # Called by the dawgfite executable, this function is really only to make a GameManager class.
    # It most of its body to GameManager in late 2018.

    print("Welcome to DawgFite " + __version__)

    if args.low_performance:
        print("Performance mode enabled")
        set_low_performance()
    if args.conventional_keys:
        Plane.PUP = pygame.K_DOWN
        Plane.PDOWN = pygame.K_UP
        Plane.PYLEFT = pygame.K_x
        Plane.PYRIGHT = pygame.K_c
        Plane.PRLEFT = pygame.K_LEFT
        Plane.PRRIGHT = pygame.K_RIGHT

    # Create a game manager class
    game = GameManager(host, port)
