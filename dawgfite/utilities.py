from __future__ import print_function

import numpy as np
import random

from version import __version__

low_performance = False  # low performance mode flag

ISLAND_DIST = 700.0    # 2 times this is the distance between the edges of two adjacent islands
ISLAND_RADIUS = 500.0

# Square meters per natural object
SQMP_HILL = 8000
SQMP_TREE = 5000

GAME_TIME = 300  # (seconds)

# Here's our message, we'll make it very specific to avoid potentially getting weird stuff.
MESSAGE = "Hello there. Do you happen to be a running server for DawgFite " + __version__ + "?"

def set_low_performance():
    """ Call this function if low performance mode is on. """

    global SQMP_HILL, low_performance

    SQMP_HILL = 20000
    low_performance = True

def input23(string):
    """
    Attempt to define an input function that is compatible with both Python 2 and Python 3.

    Essentially, it will use raw_input() if raw_input is defined and input() otherwise.
    """

    # If raw_input is defined, we are using Python 2. In this case, we must use raw_input.
    if "raw_input" in __builtins__:
        return raw_input(string)

    return input(string)  # We must be using Python 3

def mag(v):
    """ Return the magnitude of a vector """
    return np.sqrt((v ** 2).sum())

def normalize(v):
    """ Normalize a vector """
    return v / mag(v)

def a2tf(array):
    """ Convert an array to a tuple of floats. """
    return tuple([float(item) for item in array])

def time_format(time):
    """ Receive a floating point time in seconds and return a formatted string. """

    # Don't allow negative times
    if time < 0:
        return "0:00"

    minutes = int(np.floor(time / 60.0))
    seconds = int(np.floor(((time / 60.0) - minutes) * 60.0))

    minutes = str(minutes)
    if len(minutes) < 1:
        minutes = "0" + minutes

    seconds = str(seconds)
    seconds = "0" * (2 - len(seconds)) + seconds

    return minutes + ":" + seconds

def win_message(winners, winning_score):

    if len(winners) == 1:
        message = winners[0] + " won with a score of " + str(int(round(winning_score)))
    else:  # Tie
        # Can't there be a shorter way of doing this?
        message = ""
        for i, winner in enumerate(winners):
            if i == 0:
                message += winner
            else:
                message += " and " + winner
        message += " won with a score of " + str(winning_score)

    return str(message)

def naturalobj_poses(hw):
    """ Return a list of hill and poses. """

    # no_trees = 0
    # if not low_performance:
    #     no_trees = int(round((hw ** 2) / SQMP_TREE))

    # trees = []
    # for i in range(no_trees):
    #     x = random.randrange(-hw, hw)
    #     y = 0.0
    #     z = random.randrange(-hw, hw)

    #     trees.append((x, y, z))

    # return (hills, trees)

    # We might re-add trees later, but for now we will just return an empty list
    return []

def ask_number(prompt):
    """ Utility for providing a bug-proof method to determine the number of players. """

    valid = False
    while not valid:
        try:
            value = int(input23(prompt))
        except(ValueError):
            print("Not a number. Please try again.")
        else:
            if value < 2:
                print("This is a multiplayer game. Two or more please!")
            else:
                valid = True

    return value

class Shape(object):
    """ Base shape class. """

    gllist = "Unknown"

    def __init__(self):
        self.alive = True

    def die(self):
        self.alive = False

    def get_alive(self):
        return self.alive

