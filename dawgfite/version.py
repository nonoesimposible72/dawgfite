"""
Read the version from version

Other .py files usually get the version with "from version import __version__"
"""

# This assumes the current working directory is correct
reader = open("version", "r")
__version__ = reader.read().strip()
reader.close()

