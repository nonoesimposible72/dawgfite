# DawgFite II

DawgFite II is a 3D remix of my old 2D game DawgFite. It also brings multiplayer LAN networking ability. The graphics are intentionally kept very simple because the game is intended for old/low power hardware.

I'm unsure of the direction I want DawgFite's gameplay to go in and the project is now dormant. If I remember, I'll change the license back to MIT or WTF to give people ultimate freedom to continue this project or steal code from it.

## Get dependencies

First make sure you have the required dependencies. Most likely, the only one you'll have to get from source is PodSixNet; the rest will be available through your operating system's packages.

* [Python](https://www.python.org)
* [Pygame](https://www.pygame.org)
* [Numpy](https://www.numpy.org/)
* [PyOpenGL](http://pyopengl.sourceforge.net)
* [PodSixNet](https://github.com/chr15m/PodSixNet)

## Clone the repository

```
git clone https://gitlab.com/nonoesimposible72/dawgfite.git
cd dawgfite/dawgfite
```

There's no setup.py, no nothing. I probably won't start working on that until I get the beta released.

## Playing

Start the server by running the executable with the `-s` flag:

```
./dawgfite -s
```

After that, instruct each client to run dawgfite with the host that the server said it was running on. If the person running the server wants to play, they will have to have two command prompts open.

```
./dawgfite --host [ip address]
```

# License details

Technically I think I might not be exactly legal as I'm not sure what the permissions for the plane model are (why didn't I check that when I downloaded it? lol) and I downloaded the sound track directly off of some random website years ago.
